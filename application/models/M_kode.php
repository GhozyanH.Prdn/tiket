<?php
class M_kode extends CI_Model {
    function buat_kode() {
       $this->db->select('RIGHT(tiket.id,4) as kode', FALSE);
          $this->db->order_by('id','DESC');
          $this->db->limit(1);
          $query = $this->db->get('tiket');
          if($query->num_rows() <> 0){
           $data = $query->row();
           $kode = intval($data->kode) + 1;

          }
          else {
           $kode = 1;
          }
          $id = $this->session->userdata('ses');
          $date= date("his");
          $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);
          $kodejadi = "LCRIF".$date.$id.$kodemax;
          return $kodejadi;
        }
      }
?>
