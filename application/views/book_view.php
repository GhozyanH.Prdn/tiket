
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="<?php echo base_url().'assets/images/logo1.png'?>"/>
    <title>Data Peserta Lomba</title>
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
  </head>
  <body>


  <div class="container">
    <h1>Data Peserta Lomba </h1>
    <h3>Oleh Administrator&emsp;: <?php echo $this->session->userdata('ses_id');?></h3>
    <h4>Jumlah Pendapatan Total&emsp;: Rp. <?php echo number_format($total) ?></h4>
    <table border="0" >
    <colgroup>
      <col width='20px'>
      <col width='130px'>
      <col width='30px'>

    </colgroup>
    <tr>
        <th></th>
        <th></th>
    </tr>
    <tr>
        <td></td>
        <td>Seminar UINSA</td>
        <td><?php echo $uin ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Seminar Umum</td>
        <td><?php echo $umum ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Cerdas Cermat</td>
        <td><?php echo $cc ?></td>
    </tr>
    <tr>
        <td></td>
        <td>Business Plan</td>
        <td><?php echo $bp ?></td>
    </tr>
    <tr>
        <td></td>
        <td>PUBG Mobile</td>
        <td><?php echo $pubg ?></td>
    </tr>
        <tr>
        <td></td>
        <td>Dota 2</td>
        <td><?php echo $dota ?></td>
    </tr>
        <tr>
        <td></td>
        <td>Mobile Legend</td>
        <td><?php echo $ml ?></td>
    </tr>
</table>
<br>


    <div ><a href="<?php echo base_url().'index.php/input'?>" class="btn btn-success" ><span class="glyphicon glyphicon-plus"></span> Tambah Data</a>
    <a href="<?php echo base_url().'index.php/Login/logout'?>" class="btn btn-danger" ><span class="glyphicon glyphicon-log-out"></span> Logout</a></div>

    <br />
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
            <th>ID</th>
          <th>Kode Boking</th>
          <th>Penanggung Jawab</th>
          <th>Asal Sekolah</th>
          <th>Ketua TIM</th>
          <th>Phone</th>
          <th>Lomba</th>
          <th>Bayar</th>
          <th>Status</th>
          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody>
				<?php foreach($books as $book){?>
				     <tr>
				         <td><?php echo $book->id;?></td>
				         <td><?php echo $book->kode;?></td>
				         <td><?php echo $book->pj;?></td>
						 <td><?php echo $book->sekolah;?></td>
						 <td><?php echo $book->peserta1;?></td>
                <td><?php echo $book->phone;?></td>
                 <td><?php echo $book->jenis;?></td>
                 <td><?php echo number_format($book->bayar);?></td>
                 <td><?php echo $book->status;?></td>
								<td>
									<button class="btn btn-sm btn-primary" onclick="edit_book(<?php echo $book->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
									<button class="btn btn-sm btn-danger" onclick="delete_book(<?php echo $book->id;?>)"><i class="glyphicon glyphicon-trash"></i></button>
                                    <a href="<?php $id = $book->id;  echo base_url("index.php/book/cetak/$id") ?> " target="_blank"> <button class="btn btn-sm btn-warning"> <i class="glyphicon glyphicon-print"></i></button> </a>
								</td>
				      </tr>
				     <?php }?>
      </tbody>
    </table>

  </div>

  <script src="<?php echo base_url('assets/jquery/jquery-3.1.0.min.js')?>"></script>
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>


  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;


    function add_book()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_book(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('Book/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="kode"]').val(data.kode);
            $('[name="sekolah"]').val(data.sekolah);
            $('[name="email"]').val(data.email);
            $('[name="peserta1"]').val(data.peserta1);
            $('[name="peserta2"]').val(data.peserta2);
            $('[name="peserta3"]').val(data.peserta3);
            $('[name="phone"]').val(data.phone);
            $('[name="jenis"]').val(data.jenis);
            $('[name="pj"]').val(data.pj);
            $('[name="bayar"]').val(data.bayar);
            $('[name="tgl"]').val(data.tgl);
            $('[name="status"]').val(data.status);


            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data Peserta Lomba IFEST'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('Book/book_add')?>";
      }
      else
      {
        url = "<?php echo site_url('Book/book_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_book(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('Book/book_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {

               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }

  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Form Edit</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" name="id">
          <input type="hidden" name="pj">
          <input type="hidden" name="tgl">
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">KODE</label>
              <div class="col-md-9">
                <input name="kode" placeholder="............." class="form-control" type="text" readonly="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">STATUS</label>
              <div class="col-md-9">
                <select name="status"  class="form-control">
                  <option value="LUNAS">Lunas</option>
                  <option value="BATAL">Batal</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">ASAL SEKOLAH</label>
              <div class="col-md-9">
                <input name="sekolah" placeholder="............." class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">PESERTA 1</label>
              <div class="col-md-9">
								<input name="peserta1" placeholder=".............r" class="form-control" type="text">

              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">PESERTA 2</label>
              <div class="col-md-9">
                <input name="peserta2" placeholder="............." class="form-control" type="text">

              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">PESERTA 3</label>
              <div class="col-md-9">
                <input name="peserta3" placeholder="............." class="form-control" type="text">

              </div>
            </div>
						<div class="form-group">
							<label class="control-label col-md-3">PILIHAN</label>
							<div class="col-md-9">
								<input name="jenis" placeholder="............." class="form-control" type="text" readonly>

							</div>
						</div>
            <div class="form-group">
              <label class="control-label col-md-3">EMAIL</label>
              <div class="col-md-9">
                <input name="email" placeholder="............." class="form-control" type="text">

              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">NO HANPHONE</label>
              <div class="col-md-9">
                <input name="phone" placeholder="............." class="form-control" type="number">

              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-3">BAYAR</label>
              <div class="col-md-9">
                <input name="bayar" placeholder="............." class="form-control" type="number">

              </div>
            </div>

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>
