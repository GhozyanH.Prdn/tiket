<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_data');
		$this->load->model ('M_kode');
		$this->load->model ('M_lomba');
		$this->load->model('blog_model');
		$this->load->helper('url');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
}

	}

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$data['data'] = $this->M_lomba->get_kategori();
		$data['kode'] = $this->M_kode->buat_kode();
		$this->load->view('input_view',$data);
	}else{
		$this->load->view('login');
	}
	}

	function get_subkategori(){
		$id=$this->input->post('id');
		$data=$this->M_lomba->get_subkategori($id);
		echo json_encode($data);
	}

	function tambah(){
		$sekolah = $this->input->post('sekolah');
		$peserta1 = $this->input->post('peserta1');
		$peserta2 = $this->input->post('peserta2');
		$peserta3 = $this->input->post('peserta3');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$jenis = $this->input->post('jenis');
		$pj = $this->input->post('pj');
		$bayar = $this->input->post('bayar');
		$tgl = $this->input->post('tgl');
		$kode = $this->input->post('kode');

		$data = array(
			'sekolah' => $sekolah,
			'peserta1' => $peserta1,
			'peserta2' => $peserta2,
			'peserta3' => $peserta3,
			'email' => $email,
			'phone' => $phone,
			'jenis' => $jenis,
			'pj' => $pj,
			'bayar' => $bayar,
			'tgl' => $tgl,
			'kode' => $kode
			);
		$this->m_data->input_data($data,'tiket');
		redirect('Book/index');
	}
}
