<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_model extends CI_Model
{

	var $table = 'tiket';


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


public function get_all_books()
{
$this->db->from('tiket');
$query=$this->db->get();
return $query->result();
}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function book_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function book_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function total()
{
	$this->db->select_sum('bayar');
   $query = $this->db->get('tiket');
   if($query->num_rows()>0)
   {
     return $query->row()->bayar;
   }
   else
   {
     return 0;
    }
}

public function cc()
{
	$this->db->from('tiket');
	$this->db->where('jenis','CERDAS CERMAT');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function umum()
{
	$this->db->from('tiket');
	$this->db->where('jenis','SEMINAR UMUM');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function uin()
{
	$this->db->from('tiket');
	$this->db->where('jenis','SEMINAR UIN');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function bp()
{
	$this->db->from('tiket');
	$this->db->where('jenis','BUSINESS PLAN');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function pubg()
{
	$this->db->from('tiket');
	$this->db->where('jenis','PUBG Mobile');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function dota()
{
	$this->db->from('tiket');
	$this->db->where('jenis','DOTA 2');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}

public function ml()
{
	$this->db->from('tiket');
	$this->db->where('jenis','Mobile Legend');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		return $query->num_rows();
	}
	else
	{
		return 0;
	}
}
}
